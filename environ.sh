#!/bin/bash

export CLASSPATH="$CLASSPATH;$LANG_PATH/dist"

################################################################################

ronin_require_scala () {
    echo hello world >/dev/null
}

ronin_include_scala () {
    motd_text "    -> Scala    : "$CLASSPATH
}

################################################################################

ronin_setup_scala () {
    echo hello world >/dev/null
}

